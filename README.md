# Train T5 for Question & Answer
The aim is to train T5 model for Q&A using squad ita dataset

```bash
python src/run_seq2seq_qa.py \
  --model_name_or_path gsarti/it5-base \
  --tokenizer_name gsarti/it5-base \
  --dataset_name squad \
  --train_file "/media/mint/Barracuda/Datasets/squad-it/SQuAD_it-train_processed.json" \
  --validation_file "/media/mint/Barracuda/Datasets/squad-it/SQuAD_it-test_processed.json" \
  --do_train \
  --do_eval \
  --per_device_train_batch_size 3 \
  --per_device_eval_batch_size 3 \
  --predict_with_generate \
  --learning_rate 3e-5 \
  --num_train_epochs 2 \
  --max_seq_length 300 \
  --doc_stride 128 \
  --max_answer_length 30 \
  --n_best_size 5 \
  --save_steps 500 \
  --save_total_limit 3 \
  --save_strategy="steps" \
  --output_dir /media/mint/Barracuda/Models/QandA/it5-squad2
```

# Gradio UI
```
pip install gradio
python src/app.py
```

# References
* [transformers-q&a](https://github.com/huggingface/transformers/tree/master/examples/pytorch/question-answering)
* [squad-it](https://github.com/crux82/squad-it) (official dataset)
* [model trained on official squad-it](https://huggingface.co/mrm8488/umberto-wikipedia-uncased-v1-finetuned-squadv1-it)
* [squad-it](https://huggingface.co/datasets/z-uo/squad-it) (adapted dataset)
* [issue Error: The pytorch example question-answering/run_qa_beam_search.py do not work](https://github.com/huggingface/transformers/issues/14170)
