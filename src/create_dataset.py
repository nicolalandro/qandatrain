import json

from datasets import load_dataset
from tqdm import tqdm

type = 'train'
dataset_path = f'/media/mint/Barracuda/Datasets/squad-it/SQuAD_it-{type}.json'
out_path = f'/media/mint/Barracuda/Datasets/squad-it/SQuAD_it-{type}_processed.json'

extension = dataset_path.split(".")[-1]
raw_datasets = load_dataset(extension, data_files=dataset_path, field="data")

dataset = {"data": []}

i = 0
for d in tqdm(raw_datasets["train"]):
    title = d['title']
    for p in d['paragraphs']:
        context = p['context']
        for q in p['qas']:
            answers = q['answers']
            question = q['question']
            id = q['id']
            dataset['data'].append({
                "answers": answers,
                "context": context,
                "id": i,
                "question": question,
                "title": title
            })
            i += 1

with open(out_path, 'w') as f:
    json.dump(dataset, f)
