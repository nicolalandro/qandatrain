import gradio as gr
from transformers import pipeline

nlp_t2t = pipeline(
    'text2text-generation',
    model='/media/mint/Barracuda/Digitiamo/Models/T5/pec_settembre1',
    tokenizer='/media/mint/Barracuda/Digitiamo/Models/T5/pec_settembre1'
)


def get_colored_text(response, context):
    colored_string = context[:response['start']] + '<span style="color:blue">' + context[response['start']:response[
        'end']] + '</span>' + context[response['end']:]

    return colored_string


def start(question, context):
    text = question + '\n' + context
    response = nlp_t2t(text)

    return response[0]['generated_text']


face = gr.Interface(
    fn=start,
    inputs=[
        gr.inputs.Textbox(lines=1, placeholder="Question Here… "),
        gr.inputs.Textbox(lines=10, placeholder="Context Here… ")
    ],
    outputs=[
        gr.outputs.Textbox(label="Answer"),
    ]
)

face.launch(share=True)
