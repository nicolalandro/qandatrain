import gradio as gr
from transformers import pipeline

nlp_t2t = pipeline(
    'text2text-generation',
    model='z-uo/it5-squadv1-it',
    tokenizer='z-uo/it5-squadv1-it'
)


def get_colored_text(response, context):
    colored_string = context[:response['start']] + '<span style="color:blue">' + context[response['start']:response[
        'end']] + '</span>' + context[response['end']:]

    return colored_string


def start(question, context):
    text = question + '\n' + context
    response = nlp_t2t(text)

    return response[0]['generated_text']


face = gr.Interface(
    fn=start,
    inputs=[
        gr.inputs.Textbox(lines=1, placeholder="Question Here… "),
        gr.inputs.Textbox(lines=10, placeholder="Context Here… ")
    ],
    outputs=[
        gr.outputs.Textbox(label="Answer"),
    ]
)

face.launch(share=True)
